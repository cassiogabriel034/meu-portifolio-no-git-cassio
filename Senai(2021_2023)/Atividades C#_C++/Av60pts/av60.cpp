#include <iostream>

/*Desenvolva um programa onde na fun��o MAIN, deve ter um menu de escolhas para o usu�rio. Nesse
menu voc� deve apresentar tr�s op��es para o usu�rio (op��es A, B e C). Se ele escolher a op��o A,
voc� deve pedir para ele inserir um n�mero e informar se este n�mero � primo ou n�o. Se ele digitar
B,
o programa deve pedir para ele inserir um n�mero e verificar se este n�mero � �mpar ou par. Se ele
escolher a op��o C, voc� vai direcion�-lo para uma calculadora, onde dever� pedir para ele inserir dois
valores inteiros e escolher uma das seguintes op��es: Soma, Subtra��o, Multiplica��o e Divis�o. Ap�s
apresentar a resposta na tela de acordo com a escolha do usu�rio, o programa deve voltar para o menu
de escolhas. S� quando ele digitar
X, o programa deve ser encerrado*/
using namespace std;

void Dividi(int x, int y) //Aqui faz a divis�o//
{
	int calc;
	calc = x / y;
	cout << "A Divisao entre esses valores: " << calc << endl;
	
	int main();	/*Respons�vel por volta ao menu, ou seja, o Main*/
}

void Multi(int x, int y) //Aqui faz a multiplica��o//
{
	int calc;
	calc = x * y;
	cout << "A Multiplica�ao entre esses valores: " << calc << endl;
	
	int main();	/*Respons�vel por volta ao menu, ou seja, o Main*/
}


void Subtrai(int x, int y) //Aqui faz a subtra��o//
{
	int calc;
	calc = x - y;
	cout << "A Subtra�ao entre esses valores: " << calc << endl;
	
	int main(); /*Respons�vel por volta ao menu, ou seja, o Main*/
}

void Soma(int x, int y) //Aqui faz a soma//
{
	int calc;
	calc = x + y;
	cout << "A soma entre esses valores: " << calc << endl;
	
	int main(); /*Respons�vel por volta ao menu, ou seja, o Main*/
}
void Recebe_M() /*Respons�vel para inserir os valores e escolher a opera��o*/
{
	string escolha; 
	int valor;
	int valor2;
	
	cout << "Insira dois valores" << endl; /*Aqui o usuario ira insirir os valores*/
	cin >> valor;
	cin >> valor2;
	
	cout << "Agora escolha a letra da opera�ao: S eh de soma, D eh de SubtracAo, F eh de multiplicacao e G eh de divisao" << endl;
	cin >> escolha; /*Aqui o usuario ira escolhe a opera��o*/
	
	if(escolha == "S")
	{
		Soma(valor, valor2); /*Respons�vel por manda os valores por prarametro para o void Soma*/
	}
	else if(escolha == "D") /*Respons�vel por manda os valores por prarametro para o void Subtrai*/
	{
		Subtrai(valor, valor2);
	}
	else if(escolha == "F") /*Respons�vel por manda os valores por prarametro para o void Multi*/
	{
		Multi(valor, valor2);
	}
	else if(escolha == "G") /*Respons�vel por manda os valores por prarametro para o Dividi Subtrai*/
	{
		Dividi(valor, valor2);
	}
}
//---------------------------------------
void Verefica_P_N(int x) /*Respons�vel para verifica se eh par ou �mpar*/
{
	if (x % 2 == 0)
	{
		cout << x << " eh par" << endl;
	}
	else
	{
		cout << x << " eh impar" << endl;
	}
	
	int main(); /*Respons�vel por volta ao menu, ou seja, o Main*/
}

void Receber_P_N() /*Respons�vel para inserir o valor*/
{
	int valor;
	cout << "Insira uma valor" << endl;
	cin >> valor;
	
	Verefica_P_N(valor); /*Respons�vel por manda o valor por prarametro para o void Verefica_P_N e chama ele*/
}
//---------------------------------------
void Verefica_P_I(int x) /*Respons�vel para vefifica e imprimi se o valor � primo ou n�o*/
{
	int cont = 0;
	
	for(int i = 1; i <= x; i++) /*Aqui vai verifica se o valor � primo ou n�o*/
	{
		if (x % i == 0) /*A variavel auxiliar ira conta as divis�es do numero insirido em que o resto � 0 */
		{
			cont++; 
		}
	}
	
	if (cont == 2) /*Aqui vai imprimi se o valor � primo ou n�o*/
	{
		cout << x << " eh primo" << endl;
	}
	else
	{
		cout << x << " nao eh primo" << endl;
	}
	
	int main(); /*Respons�vel por volta ao menu, ou seja, o Main*/
}

void Recebe_P_I() /*Respons�vel para inserir o valor*/
{
	int valor;
	cout << "Insira uma valor" << endl;
	cin >> valor;
	
	Verefica_P_I(valor); /*Respons�vel por manda o valor por prarametro para o void Verefica_P_I e chama ele*/
}

int main(int argc, char** argv) 
{	
	string escolha;
	
	while (escolha != "X") //Respons�vel por escolhe as op��es.
	{
		cout << "----------------------------------------------------------------------------------" << endl;
     	cout << "Insira A para inserir um nUmero e informar se este nUmero eh primo ou nAo" << endl;
    	cout << "Insira B para inserir um nUmero e verificar se este numero eh impar ou par" << endl;
     	cout << "Insira C para direciona a uma calculadora, onde devere escolheres os valores e as operacoes" << endl;
    	cout << "Insira X para encerrar o programa"<< endl;
    	cin >> escolha;
    	
		if(escolha == "A") /*Respons�vel para chama void Recebe_P_I*/
		{
			Recebe_P_I();
		}
		else if(escolha == "B") /*Respons�vel para chama void Receber_P_N*/
		{
			Receber_P_N();
		}
		else if(escolha == "C") /*Respons�vel para chama void Recebe_M*/
		{
			Recebe_M();
		}
	}	
}
