#include <iostream>

/*  Ler tr�s notas de um candidato em um concurso. Calcular a m�dia ponderada, considerando
que a primeira nota tem peso 4, a segunda tem peso 2 e a terceira tem peso 5. Exibir uma
mensagem dizendo qual a m�dia do aluno e se ele foi aprovado ou reprovado. A m�dia para
aprova��o � 7. */
using namespace std;
	
void media_ponderada()
{
	int valor1;
	int valor2;
	int valor3;
	int calculo;
	
	cout << "Insira as 3 notas inteiras" << endl;
	cin >> valor1;
	cin >> valor2;
	cin >> valor3;
	
	calculo = (valor1* 4 + valor2 * 2 + valor3 * 5) / (4 + 2 + 5);
	if(calculo > 7)
	{
		cout << "O candidato esta aprovado" << endl;
	}
	else
	{
		cout << "O candidato reeprovou" << endl;
	}
}
int main(int argc, char** argv) 
{
	media_ponderada();
	return 0;
}
