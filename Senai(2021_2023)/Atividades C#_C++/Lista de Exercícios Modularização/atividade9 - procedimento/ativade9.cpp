#include <iostream>
 /* Dizemos que um n�mero natural n � pal�ndromo se o 1� algarismo de n � igual ao
seu �ltimo algarismo, o 2� algarismo de n � igual ao pen�ltimo algarismo, e assim
sucessivamente, ou seja, ler o valor de tr�s para frente ou de frente para tr�s � a
mesma coisa, por exemplo, 151. Solicite para o usu�rio inserir 3 valores inteiros e
verificar se formam um pal�ndromo*/
using namespace std;
int valor1;
int valor2;
int valor3;

void ler_valores()
{
	cout << "Faz um numero com 3 algarismos. Insira os valores" << endl;
	cin >> valor1;
	cin >> valor2;
	cin >> valor3;
}

int VereficaPalindromo()
{
	if(valor1 % valor3 == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void ImprimiPalindromo()
{
	int RecebePalindromo = VereficaPalindromo();
	if(RecebePalindromo == 1)
	{
		cout << "Eh palindromo " << endl;
	}
	else
	{
		cout << "Nao eh palindromo " << endl;
	}
}

int main(int argc, char** argv) 
{
	ler_valores();
	ImprimiPalindromo();
	return 0;
}
