#include <iostream>

/*Crie um algoritmo que o usu�rio entre com v�rios n�meros inteiros e positivos e imprima o
produto dos n�meros �mpares e a soma dos n�meros pares*/
using namespace std;
int soma = 0;
int produto = 1; 

void Leitura_e_Calculo_ValoresPositivos()
{  
    int valor;
    
    //La�o de repeti��o enquanto o valor for maior que 0
    while(valor >= 0)
    {
        if(valor % 2 == 0)
        {
            soma = soma + valor;
        }
        else
        {
            produto = produto * valor;
        }
        
        cout << "Insira um valor positivo " << endl;
        cin >> valor;
        
    }  
}

void Imprimir()
{
    cout << "Soma dos Pares " << soma << endl;
    cout << "Produto dos Impares " << produto << endl;
}

int main(int argc, char** argv) 
{
	Leitura_e_Calculo_ValoresPositivos();
    Imprimir();
	return 0;
}
