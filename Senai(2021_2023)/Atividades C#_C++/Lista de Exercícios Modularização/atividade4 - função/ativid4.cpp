#include <iostream>

/*Ler um n�mero fornecido pelo usu�rio e calcula o fatorial (n!). Utilize
obrigatoriamente Fun��es.*/
using namespace std;

int LeituraValorInteiro()
{
    int valor;// variavel local 
    
    cout << "Insira um valor :" << endl;
    cin >> valor;
    
    return valor;
}

int Calcula_fatorial()
{  
    int recebe_valor = LeituraValorInteiro(); 
    
    int fat = 1;
    
    for(int i = recebe_valor; i >= 1; i--)
    {
        fat =  fat * i;
    }

    return fat;
}

void Imprimir()
{
    int recebe_resultado_fatorial = Calcula_fatorial();
    
    cout << "O resultado do fatorial eh " << recebe_resultado_fatorial << endl;
}

int main(int argc, char** argv) 
{
	Imprimir();
	return 0;
}
