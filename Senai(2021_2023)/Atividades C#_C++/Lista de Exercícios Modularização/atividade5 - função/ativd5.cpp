#include <iostream>

/*) Crie um algoritmo que ao receber um n�mero, verifica se for �mpar ou par.*/
using namespace std;

int par_ou_impar()
{
	int valor;
	int calc;
	
	cout << "Insira um valor" << endl;
	cin >> valor;
	
	calc = valor % 2;
	return calc;
}

int main(int argc, char** argv) 
{
	int recebe_valor;
	recebe_valor = par_ou_impar();
	
	if (recebe_valor == 0)
	{
		cout << "Esse eh par" << endl;
	}
	
	else 
	{
		cout << "Esse valor eh impar" << endl;
	}
	return 0;
}
