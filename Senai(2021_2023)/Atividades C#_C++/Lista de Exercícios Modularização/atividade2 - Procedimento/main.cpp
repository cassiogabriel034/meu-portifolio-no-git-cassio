#include <iostream>

/*Escreva um algoritmo que recebe um valor e apresente na tela o seu antecessor e o
seu sucessor*/
using namespace std;

void somaantesuce()
{
	int valor; 
	
	cout << "Insira um valor inteiro" << endl;
	cin >> valor;
	
	cout << "O antecessor e o sucessor do " << valor << " ,respectivamente, eh " << valor - 1 << " e " << valor + 1 << endl;
}

int main(int argc, char** argv) 
{
	somaantesuce();
	return 0;
}
