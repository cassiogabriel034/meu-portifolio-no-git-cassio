#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

/*Nome do programador: Cassio Gabriel
Problema: Receba um valor do usuario(a)*/
using namespace std;

int valor; //Essa � uma variavel que recebera um valor

int main(int argc, char** argv) 
{
	
	cout << "Querido(a) usuario(a), insira um valor inteiro: " << endl; //� um comado que vai imprimir uma mensagem na tela
	cin >> valor; //� um comado, que recebera um valor do usuario(a)
	
	return 0;
}
