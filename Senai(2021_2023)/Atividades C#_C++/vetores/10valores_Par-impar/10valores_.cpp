#include <iostream>

/* Crie um vetor com 10 valores inteiros iniciais.
? Verifique se existe algum elemento par e imprima
O n�mero par.
? Utilize Modulariza��o..*/
using namespace std;

void Verifica(int valores[10])
{
	cout << "Os pares sao:" << endl;
	for(int i = 0; i <= 9; i++)
	{
		if (valores[i] % 2 == 0)
		{
			cout << valores[i] << endl;
		}
	}
}

void Recebe()
{
	int valores[10];
	
	for(int i = 0; i <= 9; i++)
	{
		cout << "Insira 10 valores inteiros" << endl;
		cin >> valores[i];
	}
	
	Verifica(valores);
}
int main(int argc, char** argv) 
{
	Recebe();
	return 0;
}
