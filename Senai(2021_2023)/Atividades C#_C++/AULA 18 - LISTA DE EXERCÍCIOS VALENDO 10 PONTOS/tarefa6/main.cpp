#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;
int valor1;
int divs2;
int resto1;
int resto2;

int main(int argc, char** argv) 
{
	int div = 1;
	divs2 = 0;
	cout << "Digite um valor" << endl;
	cin >> valor1;
	
	if(valor1 > 0)
	{
		while(div <= valor1)
		{
			resto1 = valor1 % 1;
			resto2 = valor1 % divs2;
			divs2++;
			
			div++;
		}	
		if(resto1 == 0 && resto2 == 0)
		{
			cout << "O valor " << valor1 << " eh primo" << endl;
		} 
		else
		{
			cout << "O valor " << valor1 << " nao eh primo" << endl;
		}	
	}
	
	else
	{
		cout << "Valor negativo ou igual a zero" << endl;
	}
	return 0;
}
