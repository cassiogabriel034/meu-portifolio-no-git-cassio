#include <iostream>

/* Desenvolver um programa que leia um valor num�rico inteiro e fa�a a apresenta��o desse
valor caso seja divis�vel por 4 e 5. N�o sendo divis�vel por 4 e 5, o programa deve apresentar a
mensagem �Valor n�o � divis�vel por 4 e 5�.*/
using namespace std;
	
int valor1;
int resto1;
int resto2;
int main(int argc, char** argv) 
{
	cout << "Insira um valor inteiro divisivel por 4 e 5 " << endl;
	cin >> valor1;
	resto1 = valor1 %4;
	resto2 = valor1 % 5;
	
    if (resto1 == 0 && resto2 == 0)
    {
    	cout << "Esse valor eh divisivel pelos dois" << endl;
	}	
	else
	{
		cout << "Esse valor nao eh divisivel pelos dois" << endl;
	}
	return 0;
}
