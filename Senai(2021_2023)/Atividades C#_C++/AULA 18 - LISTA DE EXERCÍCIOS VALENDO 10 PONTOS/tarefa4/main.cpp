#include <iostream>

/*  Desenvolva um algoritmo onde o usu�rio vai inserir v�rios valores inteiros positivos e o
programa retorna quantos n�meros pares foram inseridos. (DICA: SE INSERIR UM VALOR
NEGATIVO, O PROGRAMA DEVE SER ENCERRADO) */
using namespace std;
int valor1;
string texto;
int quat_Pt;

int main(int argc, char** argv) 
{
	cout << "Usuario digiti sim ou nao para iniciar o programa" << endl;
	cin >> texto;
	quat_Pt = 0;
	
	while(texto != "nao")
	{
		cout << "Insira um valor inteiro" << endl;
		cin >> valor1;
	
	 	if(valor1 < 0)
	 	{
	 		cout << "O programa encerrou por o valor ser negativo" << endl;
	 		system("pause");
		}
		
		else 
		{
			quat_Pt++;
			cout << "A quantidade de valores positivos eh " << quat_Pt << endl;
		}
		
		cout << "Deseja continuar?" << endl;
		cin >> texto;
	}	         	 
	
	return 0;
}
