#include <iostream>

/*Elabore um algoritmo modularizado que recebe dois n�mero e o usu�rio escolhe qual opera��o deseja visualizar entre:
A soma dos n�meros e a m�dia aritm�tica.
O produto do primeiro n�mero pelo quadrado do segundo.
O quadrado do primeiro n�mero.
O m�dulo entre os dois n�meros. */
using namespace std;

void Operacao(int x, int y)
{
	int opcao;
	cout << "####################################################################################" << endl;
	cout << "# Usuario vc tem 4 opcoes de operacoes: (1)A soma dos numeros e a media aritmetica #" << endl;
	cout << "# (2)O produto do primeiro numero pelo quadrado do segundo                         #" << endl;
	cout << "# (3)O quadrado do primeiro numero e (4)O modulo entre os dois numeros             #" << endl;
	cout << "#                                                                                  #" << endl;
	cout << "#  Digiti o numero para escolher a operacao                                        #" << endl;
	cout << "####################################################################################" << endl;
	cin >> opcao;
	
	if (opcao == 1)
	{
		cout << "# A soma entre eles eh " << x + y << " e a media eh " << (x + y) / 2 << " #" << endl;
	}
	else if (opcao == 2)
	{
		cout << "# O produto do primeiro numero pelo quadrado do segundo eh " << x * (y * y) << " #" << endl;
	}
	else if (opcao == 3)
	{
		cout << "# O quadrado do primeiro numero eh " << x * x << " #" << endl;
	}
	else if (opcao == 4)
	{
		cout << "O modulo entre os dois numeros eh " << x % y << " #" << endl;
	}
}

void Ler_Valor()
{
	int valor1;
	int valor2;
	cout << "################################" << endl;
	cout << "# Insira dois valores inteiros #" << endl;
	cout << "################################" << endl;
	cin >> valor1;
	cin >> valor2;
	
	Operacao(valor1, valor2);
}
int main(int argc, char** argv) 
{
	Ler_Valor();
	return 0;
}
