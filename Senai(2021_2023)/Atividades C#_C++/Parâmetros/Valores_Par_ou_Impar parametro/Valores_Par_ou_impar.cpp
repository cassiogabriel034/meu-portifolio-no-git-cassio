#include <iostream>

/*Hora de Praticar

? Crie um programa onde o usu�rio vai inserir 10 valores inteiros.
? Uma outra fun��o ou procedimento, vai receber esses valores e verificar
se ele � impar ou par imprimindo na tela o resultado.
CONDI��ES:
?N�O UTILIZE VARI�VEIS GLOBAIS.
NA MAIN DEVE TER APENAS UMA CHAMADA DE
?FUN��O OU PROCEDIMENTO.*/
using namespace std;

void Par_Impar(int verifica, int para)
{
	int calculo;
	calculo = verifica % 2;
	
	if (calculo == 0)
	{
		cout << "Esse valor eh par" << endl;
	}
	else
	{
		cout << "Esse valor eh impar" << endl;
	}
		
	if (para == 10)
	{
		cout << "Obrigado por usa meu codigo" << endl;
	}
	
}

void Recebe()
{
	for(int i = 1; i <= 10; i++)
	{
		int valor1;
		
		cout << "Insira um valor inteiro" << endl;
		cin >> valor1;
		
		Par_Impar(valor1, i);
	}
}
int main(int argc, char** argv) 
{
	Recebe();
	return 0;
}
