#include <iostream>

/*Crie M�dulos para o seguinte problema: Crie um algoritmo que ao receber um n�mero, verifica se � impar ou par.
Utilize obrigatoriamente passagem por par�metro e n�o pode usar vari�veis globais.
*/
using namespace std;

void Impar_Par(int recebeValor)
{
	if (recebeValor % 2 == 0)
	{
		cout << recebeValor << " eh par" << endl;
	}
	else
	{
		cout << recebeValor << " eh impar" << endl;
	}	
}

void Ler_Valor()
{
	int valor;
	cout << "Insira um valor inteiro" << endl;
	cin >> valor;
	
	Impar_Par(valor);
}

int main(int argc, char** argv) 
{
	Ler_Valor();
	return 0;
} 



