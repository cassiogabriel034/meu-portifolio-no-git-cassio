/*
CRIEM UM PROCEDIMENTO QUE RECEBE UM NOME 
CRIE UM OUTRO PROCEDIMENTO QUE IMPRIMA O NOME
OBRIGAT�RIO O USO DE PASSAGEM POR PAR�METRO.
*/

#include <iostream>

using namespace std;

string nome;

//Procedimento que recebe uma informa��o por par�metro
//e s� imprimi essa informa��o
void ImprimiNome(string recebe_informacao)
{
    cout << " Bem Vindo(a) " << recebe_informacao << endl;  
}

//Procedimento que recebe uma informa��o de entrada e passa para o procedimento de imprimir
void RecebeNome()
{
    cout << "Insira seu nome " << endl;
    cin >> nome;  
    
    ImprimiNome(nome);
}

//Bloco principal
int main()
{
    //Chamando o procedimento de recebeNome.
    //Reparem que n�o chamei o procediento de imprimir aqui na MAIN, pq ele j� est� sendo Chamando dentro do procedimento RecebeNome
    RecebeNome();    

    return 0;
}

