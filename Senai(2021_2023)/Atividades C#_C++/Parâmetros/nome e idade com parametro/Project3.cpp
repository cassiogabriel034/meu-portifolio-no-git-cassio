#include <iostream>

/* Hora de Praticar
 Fa�a um programa onde o usu�rio vai inserir seu nome e idade. 
 Passe essas informa��es para fun��o ou procedimento que vai imprimir 
essas informa��es.
 A MAIN deve chamar a fun��o ou o procedimento respons�vel para a 
resolu��o do exerc�cio. */
using namespace std;
void Imprime_nome_idade(string imprimi_nome, int imprimi_idade)
{
	cout << "Vc " << imprimi_nome << " tem " << imprimi_idade << " anos" << endl;
}

void Recebe_dados()
{
	int idade;
	string nome;
	
	cout << "Insira seu nome e sua idade" << endl;
	cin >> nome;
	cin >> idade;
	
	Imprime_nome_idade(nome, idade);
}

int main(int argc, char** argv) {
	Recebe_dados();

	return 0;
}
