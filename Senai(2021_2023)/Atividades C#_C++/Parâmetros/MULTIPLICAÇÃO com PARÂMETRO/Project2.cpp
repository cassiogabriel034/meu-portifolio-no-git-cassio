/******************************************************************************

CRIEM UM PROCEDIMENTO QUE RECEBE UM VALOR INTEIRO
CRIE OUTRO PROCEDIMENTO QUE FAÇA A MULTIPLICAÇÃO DESSE VALOR
CRIE OUTRO PROCEDIMENTO QUE IMPRIMA O RESULTADO DA MULTIPLICAÇÃO
OBRIGATÓRIO O USO DE PASSAGEM POR PARÂMETRO.

*******************************************************************************/
#include <iostream>
using namespace std;

void Imprime(int imprimi)
{
	cout << "A multiplicação eh " << imprimi << endl;
}

void Calculo(int x, int y)
{
	int calculo;
	calculo = x * y;
	
	Imprime(calculo);
}

void RecebeValores()
{
	int valor1;
	int valor2;
	
	cout << "Insira os dois valores inteiros para a multiplicaçao" << endl;
	cin >> valor1;
	cin >> valor2;
	
	Calculo(valor1, valor2);
}




int main()
{
    RecebeValores();

    return 0;
}

