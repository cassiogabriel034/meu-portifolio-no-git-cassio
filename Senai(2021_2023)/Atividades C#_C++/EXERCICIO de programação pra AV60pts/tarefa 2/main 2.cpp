#include <iostream>

/*Desenvolva um programa onde na fun��o MAIN, deve ter um menu de escolhas para o usu�rio. Nesse menu
voc� deve apresentar duas op��es para o usu�rio. Se ele digitar o n�mero 1, voc� deve pedir para ele inserir
dois valores inteiros. Passe esses valores por par�metro para um procedimento que vai realizar a soma desses
valores. O resultado da opera��o, passe para um procedimento respons�vel por imprimir.
Se o usu�rio digitar 2, voc� deve pedir para ele inserir dois valores inteiros e passar por par�metro para um
procedimento que vai realizar a opera��o de divis�o entre esses valores. O resultado dessa opera��o dever� ser
passado*/
using namespace std;

void Soma(int x, int y) 
{
	cout << "A soma entre " << x << " e " << y << " eh " << x + y << endl; //Aqui ocorrer� a soma caso o usario digiti 1//
}

void Divisao(int x, int y)
{
	cout << "A divisao entre " << x << " e " << y << " eh " << x / y << endl; //Aqui ocorrer� a Divisao caso o usario digiti 2//
}

int main(int argc, char** argv) 
{
	int escolha;
	int valor1;
	int valor2;
	
	cout << "Insira 1 pra soma dois valores inteiros ou 2 dividir dois numeros inteiros" << endl; //Aqui o usuario ira escolhe a op��o//
	cin >> escolha;
	
	if (escolha == 1)
	{
		cout << "Insira dois valores inteiros" << endl; //Nessa parte o programa ira pedi valores//
		cin >> valor1; // e aqui receber os numeros insiridos//
		cin >> valor2; // e aqui receber os numeros insiridos//              
	 	
		Soma(valor1, valor2); //Aqui ira manda os valores pra o procedimento Soma e chama ele//
	}
	else
	{
		cout << "Insira dois valores inteiros" << endl; //Nessa parte o programa ira pedi valores//
		cin >> valor1; //Nessa parte o programa ira pedi valores//
		cin >> valor2; //Nessa parte o programa ira pedi valores//
		 
		Divisao(valor1, valor2); //Aqui ira mandao os valores pra o procedimento Divisao e chama ele//
	}
	
}
