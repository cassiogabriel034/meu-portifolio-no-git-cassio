#include <iostream>

/*Fa�a um programa onde o usu�rio vai inserir um n�mero inteiro. Passe esse valor por
par�metro para um procedimento que vai realizar a verifica��o se este n�mero � primo
ou n�o. Um n�mero primo tem apenas 2 divisores: 1 e ele mesmo! O n�mero 1 n�o �
primo!!!)*/
using namespace std;
void Imprimi(int y, int x) /*esse Procedimento ira imprimir se o valor � primo ou nao*/
{
	if (y == 2)
	{
		cout << x << " eh primo" << endl;
	}
	else
	{
		cout << x << " nao eh primo" << endl;
	}
}

void Verifica(int x) /*Esse Procedimento ira verifica se o valor � primo*/
{
	int cont = 0;
	
	for(int i = 1; i <= x; i++)
	{
		if (x % i == 0)
		{
			cont++; /*A variavel auxiliar ira conta as divis�es do numero insirido em que o resto � 0 */
		}
	}
	
	Imprimi(cont, x); /*Aqui ira manda o valor da variavel auxiliar e o valor inserido pra Procedimento Imprimi*/
}

void Receber() /*Esse Procedimento ser� responsavel por receber o valor insirido pelo usuario*/
{
	int valor;
	
	cout << "Insira um valor inteiro" << endl; /*Aqui o Usu�rio ira l� a mensagem*/
	cin >> valor; /*Aqui ira receber o numero do Usu�rio*/
	
	Verifica(valor); /*Aqui ira manda o valor pra Procedimento Verifica*/
}

int main(int argc, char** argv) 
{
	Receber();
	return 0;
}

