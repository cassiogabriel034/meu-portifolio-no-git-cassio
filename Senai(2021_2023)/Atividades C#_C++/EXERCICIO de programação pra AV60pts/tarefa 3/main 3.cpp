#include <iostream>

/*Fa�a um programa que preencha um vetor com 10 n�meros inteiros. Ap�s preencher o vetor, Apresente a quantidade de
valores pares inseridos nesse vetor. Depois apresente a quantidade de valores �mpares e por fim, apresente a quantidade de
n�mero*/
using namespace std;
void Imprimi(int x, int y) /*Esse Procedimento ira mostra a quantidade de pares de impares ao usuario*/
{
	cout << "A quantidade de pares eh " << x << endl; 
	cout << "A quantidade de impares eh " << y << endl;
}

void Verifica(int valores[10]) /*Esse Procedimento ira contar  a quantidade de pares e de impares*/
{
	int cont_par = 0;
	int cont_impar = 0;
	
	for(int i = 0; i <= 9; i++)
	{
		if (valores[i] % 2 == 0)
		{
			cont_par++; /*Variavel auxiliar responsavel por contar  os pares*/
		}
		else
		{
			cont_impar++; /*Variavel auxiliar responsavel por contar os impares*/
		}		
	}
	
	Imprimi(cont_par, cont_impar); /*Aqui ira manda a quantidade de pares e de impares pra o Procedimento Imprimi e chama ele*/
}

void Recebe() /*Esse Procedimento ser�  responsavel por receber os numeros insiridos pelo usuario*/
{
	int valores[10];
	
	for(int i = 0; i <= 9; i++)
	{
		cout << "Insira 10 valores inteiros" << endl;
		cin >> valores[i];
	}
	
	Verifica(valores); /*Aqui ira manda o vetor valores para o Procedimento Verifica e chama ele*/
}


int main(int argc, char** argv) 
{
	Recebe(); 
	return 0;
}
