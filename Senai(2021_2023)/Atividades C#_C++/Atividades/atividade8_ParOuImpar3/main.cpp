#include <iostream>
using namespace std;
/*
UTILIZANDO A ESTRUTURA DO FOR,
CRIE UM PROGRAMA QUE RECEBA 5 VALORES INTEIROS
E VERIFIQUE QUANTOS S�O IMPARES E QUANTOS S�OS PARES.
IMPRIMA A QUANTIDADE DE PARES E IMPARES.
*/
int valor1;
int pares = 0;
int impares = 0;

int main(int argc, char** argv) 
{
	for(int i = 1; i <= 5; i++)
	{
		cout << "Insira 5 valores" << endl;
		cin >> valor1;
	     	
		if(valor1 % 2 == 0)
		{
			pares++;
		}
	 	
		else
		{
			impares++;
		}
	 	
		cout << "Os pares sao " << pares << " numeros" << endl;
		cout << "Os impares sao " << impares << " numeros" << endl;
		cout << "--------------------------------------------------" << endl;
	}
	return 0;
}
