#include <iostream>
using namespace std;
/*
UTILIZANDO A ESTRUTURA DO FOR.
CRIE UM PROGRAMA QUE IMPRIMA A MENSAGEM "BOM DIA, SENAI!", 10x na tela.
*/

int main(int argc, char** argv) {
	
	for(int i = 1; i <= 10; i++)
	{
		cout << "bom dia, senai! " << i << endl;
	}
	return 0;
}
