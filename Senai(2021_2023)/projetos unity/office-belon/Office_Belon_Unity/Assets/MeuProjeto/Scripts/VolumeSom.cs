using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSom : MonoBehaviour
{
    public static VolumeSom Instance;
    
    public Slider volumeMusica;
    public AudioSource soundMaster;

    private void Awake()
    {
        Instance = this;
    }

    /*void Start()
    {
        volumeMusica.value = 0;
    }*/

    void Update()
    {
        soundMaster.GetComponent<AudioSource>().volume = volumeMusica.value;
    }
}
