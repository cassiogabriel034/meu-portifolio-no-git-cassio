using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPuzzle : MonoBehaviour
{
    public static ControlPuzzle Instance;

    private void Awake()
    {
        Instance = this;
    }
    public bool vitoriaPuzzleHac1 = false;
    public bool vitoriaPuzzleHac2 = false;

    [SerializeField] GameObject canvaMonitor;

    [SerializeField] GameObject puzzleLabirito;
    [SerializeField] GameObject puzzle7Erros;

    [SerializeField] GameObject telaAzulHac1;
    [SerializeField] GameObject telaAzulHac2;


    /*private void Update()
    {      ControlAparênciaEmail();  }*/

    public void Btn_IniciarPuzzle1()
    {
        canvaMonitor.gameObject.SetActive(false);
        puzzleLabirito.gameObject.SetActive(true);
    }

    public void Btn_IniciarPuzzle2()
    {
        canvaMonitor.gameObject.SetActive(false);
        puzzle7Erros.gameObject.SetActive(true);
    }


    public void ControlAparênciaEmail()
    {
        if(vitoriaPuzzleHac1 == true)
        {
            puzzleLabirito.gameObject.SetActive(false);
            canvaMonitor.gameObject.SetActive(true);
            telaAzulHac1.gameObject.SetActive(false);

        }

        if (vitoriaPuzzleHac2 == true)
        {
            puzzle7Erros.gameObject.SetActive(false);
            canvaMonitor.gameObject.SetActive(true);
            telaAzulHac2.gameObject.SetActive(false);
        }
    }
}
