using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vereficacao : MonoBehaviour
{
    [SerializeField] GameObject menuVitortia;
    [SerializeField] GameObject menuDerrota;
    [SerializeField] GameObject respostaCerta;
    [SerializeField] GameObject respostaErrada;

    [Header("Btns")]
    [SerializeField] GameObject btnPesquisa;
    [SerializeField] GameObject btnEmail;
    [SerializeField] GameObject btnfontesConfiaveis;
    [SerializeField] GameObject btnVerdadeiro;
    [SerializeField] GameObject btnFalso;

    private void Update()
    {
        
    }

    public void Acertou()
    {
        menuVitortia.SetActive(true);

        btnEmail.SetActive(false);
        btnFalso.SetActive(false);
        btnfontesConfiaveis.SetActive(false);
        btnPesquisa.SetActive(false);
        btnVerdadeiro.SetActive(false);
    }

    public void Errou()
    {
        menuDerrota.SetActive(true);
        
        btnEmail.SetActive(false);
        btnFalso.SetActive(false);
        btnfontesConfiaveis.SetActive(false);
        btnPesquisa.SetActive(false);
        btnVerdadeiro.SetActive(false);
        
    }

}
