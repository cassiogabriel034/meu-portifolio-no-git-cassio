using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlCincosErros : MonoBehaviour
{
    [SerializeField] int errosEncoontrado = 0;

    [SerializeField] GameObject btnErroUm;
    [SerializeField] GameObject btnErroDois;
    [SerializeField] GameObject btnErroTres;
    [SerializeField] GameObject btnErroQuatro;
    [SerializeField] GameObject btnErroCinco;
    [SerializeField] GameObject btnErroSeis;
    [SerializeField] GameObject btnErroSete;

    [SerializeField] Sprite imageErroUm;
    [SerializeField] Sprite imageErroDois;
    [SerializeField] Sprite imageErroTres;
    [SerializeField] Sprite imageErroQuatro;
    [SerializeField] Sprite imageErroCinco;
    [SerializeField] Sprite imageErroSeis;
    [SerializeField] Sprite imageErroSete;

    [SerializeField] Image erroUm;
    [SerializeField] Image erroDois;
    [SerializeField] Image erroTres;
    [SerializeField] Image erroQuarto;
    [SerializeField] Image erroCinco;
    [SerializeField] Image erroSeis;
    [SerializeField] Image erroSete;

    int numeroBtnApertado = 0;
    [SerializeField] float tempoEspera;

    bool vitoria7Erros;

    public void Btn_ErroUM(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroUm.gameObject.SetActive(false);
    }

    public void Btn_ErroDois(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroDois.gameObject.SetActive(false);
    }

    public void Btn_ErroTres(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroTres.gameObject.SetActive(false);
    }

    public void Btn_ErroQuatro(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroQuatro.gameObject.SetActive(false);
    }
    public void Btn_ErroCinco(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroCinco.gameObject.SetActive(false);
    }

    public void Btn_ErroSeis(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroSeis.gameObject.SetActive(false);
    }

    public void Btn_ErroSete(int nBtnEr)
    {
        errosEncoontrado = errosEncoontrado + 1;
        numeroBtnApertado = nBtnEr;
        AtivacaoDosSpritesDosErros();
        btnErroSete.gameObject.SetActive(false);
    }

    IEnumerator VitoriaPuzzle(float tempo)
    {
        yield return new WaitForSeconds(tempo);
        
        ControlPuzzle.Instance.ControlAparênciaEmail();
    }

    void AtivacaoDosSpritesDosErros()
    {
        if (errosEncoontrado == 1)
        {
            if (numeroBtnApertado == 1)
            {
                erroUm.sprite = imageErroUm;
                erroUm.color = Color.white;
            }
            else if (numeroBtnApertado == 2)
            {
                erroDois.sprite = imageErroUm;
                erroDois.color = Color.white;
            }
            else if (numeroBtnApertado == 3)
            {
                erroTres.sprite = imageErroUm;
                erroTres.color = Color.white;
            }
            else if (numeroBtnApertado == 4)
            {
                erroQuarto.sprite = imageErroUm;
                erroQuarto.color = Color.white;
            }
            else if (numeroBtnApertado == 5)
            {
                erroCinco.sprite = imageErroUm;
                erroCinco.color = Color.white;
            }
            else if (numeroBtnApertado == 6)
            {
                erroSeis.sprite = imageErroUm;
                erroSeis.color = Color.white;
            }
            else if (numeroBtnApertado == 7)
            {
                erroSete.sprite = imageErroUm;
                erroSete.color = Color.white;
            }
        }
        if (errosEncoontrado == 2)
        {
            if (numeroBtnApertado == 1)
            {
                erroUm.sprite = imageErroDois;
                erroUm.color = Color.white;
            }
            else if (numeroBtnApertado == 2)
            {
                erroDois.sprite = imageErroDois;
                erroDois.color = Color.white;
            }
            else if (numeroBtnApertado == 3)
            {
                erroTres.sprite = imageErroDois;
                erroTres.color = Color.white;
            }
            else if (numeroBtnApertado == 4)
            {
                erroQuarto.sprite = imageErroDois;
                erroQuarto.color = Color.white;
            }
            else if (numeroBtnApertado == 5)
            {
                erroCinco.sprite = imageErroDois;
                erroCinco.color = Color.white;
            }
            else if (numeroBtnApertado == 6)
            {
                erroSeis.sprite = imageErroDois;
                erroSeis.color = Color.white;
            }
            else if (numeroBtnApertado == 7)
            {
                erroSete.sprite = imageErroDois;
                erroSete.color = Color.white;
            }
        }
        if (errosEncoontrado == 3)
        {
            if (numeroBtnApertado == 1)
            {
                erroUm.sprite = imageErroTres;
                erroUm.color = Color.white;
            }
            else if (numeroBtnApertado == 2)
            {
                erroDois.sprite = imageErroTres;
                erroDois.color = Color.white;
            }
            else if (numeroBtnApertado == 3)
            {
                erroTres.sprite = imageErroTres;
                erroTres.color = Color.white;
            }
            else if (numeroBtnApertado == 4)
            {
                erroQuarto.sprite = imageErroTres;
                erroQuarto.color = Color.white;
            }
            else if (numeroBtnApertado == 5)
            {
                erroCinco.sprite = imageErroTres;
                erroCinco.color = Color.white;
            }
            else if (numeroBtnApertado == 6)
            {
                erroSeis.sprite = imageErroTres;
                erroSeis.color = Color.white;
            }
            else if (numeroBtnApertado == 7)
            {
                erroSete.sprite = imageErroTres;
                erroSete.color = Color.white;
            }
        }
        if (errosEncoontrado == 4)
        {
            if (numeroBtnApertado == 1)
            {
                erroUm.sprite = imageErroQuatro;
                erroUm.color = Color.white;
            }
            else if (numeroBtnApertado == 2)
            {
                erroDois.sprite = imageErroQuatro;
                erroDois.color = Color.white;
            }
            else if (numeroBtnApertado == 3)
            {
                erroTres.sprite = imageErroQuatro;
                erroTres.color = Color.white;
            }
            else if (numeroBtnApertado == 4)
            {
                erroQuarto.sprite = imageErroQuatro;
                erroQuarto.color = Color.white;
            }
            else if (numeroBtnApertado == 5)
            {
                erroCinco.sprite = imageErroQuatro;
                erroCinco.color = Color.white;
            }
            else if (numeroBtnApertado == 6)
            {
                erroSeis.sprite = imageErroQuatro;
                erroSeis.color = Color.white;
            }
            else if (numeroBtnApertado == 7)
            {
                erroSete.sprite = imageErroQuatro;
                erroSete.color = Color.white;
            }
        }
        if(errosEncoontrado == 5)
        {
                if (numeroBtnApertado == 1)
                {
                    erroUm.sprite = imageErroCinco;
                    erroUm.color = Color.white;
                }
                else if (numeroBtnApertado == 2)
                {
                    erroDois.sprite = imageErroCinco;
                erroDois.color = Color.white;
            }
                else if (numeroBtnApertado == 3)
                {
                    erroTres.sprite = imageErroCinco;
                erroTres.color = Color.white;
            }
                else if (numeroBtnApertado == 4)
                {
                    erroQuarto.sprite = imageErroCinco;
                erroQuarto.color = Color.white;
            }
                else if (numeroBtnApertado == 5)
                {
                    erroCinco.sprite = imageErroCinco;
                erroCinco.color = Color.white;
            }
                else if (numeroBtnApertado == 6)
                {
                    erroSeis.sprite = imageErroCinco;
                erroSeis.color = Color.white;
            }
                else if (numeroBtnApertado == 7)
                {
                    erroSete.sprite = imageErroCinco;
                erroSete.color = Color.white;
            }
        }
        if(errosEncoontrado == 6)
        {
            if (numeroBtnApertado == 1)
            {
                erroUm.sprite = imageErroSeis;
                erroUm.color = Color.white;
            }
            else if (numeroBtnApertado == 2)
            {
                erroDois.sprite = imageErroSeis;
                erroDois.color = Color.white;
            }
            else if (numeroBtnApertado == 3)
            {
                erroTres.sprite = imageErroSeis;
                erroTres.color = Color.white;
            }
            else if (numeroBtnApertado == 4)
            {
                erroQuarto.sprite = imageErroSeis;
                erroQuarto.color = Color.white;
            }
            else if (numeroBtnApertado == 5)
            {
                erroCinco.sprite = imageErroSeis;
                erroCinco.color = Color.white;
            }
            else if (numeroBtnApertado == 6)
            {
                erroSeis.sprite = imageErroSeis;
                erroSeis.color = Color.white;
            }
            else if (numeroBtnApertado == 7)
            {
                erroSete.sprite = imageErroSeis;
                erroSete.color = Color.white;
            }
        }
        if (errosEncoontrado == 7)
        {
            if (numeroBtnApertado == 1)
            {
                erroUm.sprite = imageErroSete;
                erroUm.color = Color.white;
            }
            else if (numeroBtnApertado == 2)
            {
                erroDois.sprite = imageErroSete;
                erroDois.color = Color.white;
            }
            else if (numeroBtnApertado == 3)
            {
                erroTres.sprite = imageErroSete;
                erroTres.color = Color.white;
            }
            else if (numeroBtnApertado == 4)
            {
                erroQuarto.sprite = imageErroSete;
                erroQuarto.color = Color.white;
            }
            else if (numeroBtnApertado == 5)
            {
                erroCinco.sprite = imageErroSete;
                erroCinco.color = Color.white;
            }
            else if (numeroBtnApertado == 6)
            {
                erroSeis.sprite = imageErroSete;
                erroSeis.color = Color.white;
            }
            else if (numeroBtnApertado == 7)
            {
                erroSete.sprite = imageErroSete;
                erroSete.color = Color.white;
            }
            ControlPuzzle.Instance.vitoriaPuzzleHac2 = true;
            StartCoroutine(VitoriaPuzzle(tempoEspera));
        }
    }
}
