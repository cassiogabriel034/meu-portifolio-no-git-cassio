using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class MouseClicarNoObjeto : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private RectTransform _transform;
    //[SerializeField] private Canvas _canvas;
    [SerializeField] private CanvasGroup _grupo;

    private void Awake()
    {
        _transform = GetComponent<RectTransform>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //_canvasGroup.blocksRaycasts = false;
        _grupo.alpha = 0.3f;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //_canvasGroup.blocksRaycasts = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _grupo.alpha = 1f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        // seta a posi��o da imagem para a posi��o que o mouse vai.
        _transform.anchoredPosition += eventData.delta;
    }
}

