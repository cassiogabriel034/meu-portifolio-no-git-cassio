using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSlide : MonoBehaviour
{
    public NumberBox boxPrefab;
    public NumberBox[,] boxes = new NumberBox[4,4];
    public Sprite[] sprites;

    private void Start()
    {
        Init();
    }

    void Init()
    {
        int n = 0;
        for(int y = 3; y>=0; y--)
            for(int x = 0; x<4; x++)
            {
                NumberBox box = Instantiate(boxPrefab, new Vector2(x,y), Quaternion.identity);
                box.Init(x, y, n + 1, sprites[n], ClickToSwap);
                boxes[x,y] = box;
                n++;   
            }
    }

    void ClickToSwap(int x, int y)
    {
        int dx = GetDx(x, y);
        int dy = GetDy(x, y);

        var from = boxes[x,y];
        var target = boxes[x + dx,y + dy];

        boxes[x,y] = target;
        boxes[x + dx,y + dy] = from;

        from.UpdatePos(x + dx, y + dy);
        target.UpdatePos(x, y);
       
    }

    int GetDx(int x, int y)
    {
        if (x < 3 && boxes[x + 1, y].IsEmpety())
        {
            return 1;
        }

        if (x >0 && boxes[x - 1, y].IsEmpety())
        {
            return -1;
        }
        return 0;
    }

    int GetDy(int x , int y)
    {
        if (y < 3 && boxes[ x, y + 1].IsEmpety())
        {
            return 1;
        }

        if (y > 0 && boxes[ x, y - 1].IsEmpety())
        {
            return -1;
        }
        return 0;

    }
    

    

}
