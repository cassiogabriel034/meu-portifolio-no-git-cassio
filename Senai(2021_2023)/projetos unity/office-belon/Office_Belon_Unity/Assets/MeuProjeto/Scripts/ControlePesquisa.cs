using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlePesquisa : MonoBehaviour
{
    [SerializeField] GameObject btnPesquisa;
    
    [Header("Saude")]
    [SerializeField] GameObject pesquisaSobreSaude;
    [SerializeField] GameObject pesquisaS1;
    [SerializeField] GameObject pesquisaS2;
    [SerializeField] GameObject pesquisaS3;

    [Header("Politica")]
    [SerializeField] GameObject pesquisaSobrePolitica;
    [SerializeField] GameObject pesquisaP1;
    [SerializeField] GameObject pesquisaP2;
    [SerializeField] GameObject pesquisaP3;

    [Header("Economia")]
    [SerializeField] GameObject pesquisaSobreEconomia;
    [SerializeField] GameObject pesquisaE1;
    [SerializeField] GameObject pesquisaE2;
    [SerializeField] GameObject pesquisaE3;


    public void PesquisaSaude()
    {
        pesquisaSobreSaude.SetActive(true);   
    }

    public void PesquisaEconomia()
    {
        pesquisaSobreEconomia.SetActive(true);
    }

    public void PesquisaPolitica()
    {
        pesquisaSobrePolitica.SetActive(true);
    }

    public void SairSaude()
    {
        pesquisaSobreSaude.SetActive(false);
    }

    public void SairPolitica()
    {
        pesquisaSobrePolitica.SetActive(false) ;
    }
    public void SairEconomia()
    {
        pesquisaSobreEconomia.SetActive(false) ;   
    }

    //Pesquisas Sobre saude
    public void PesquisaS1()
    {
        pesquisaS1.SetActive(true);
    } 
    public void PesquisaS2()
    {
        pesquisaS2.SetActive(true);
    } 
    public void PesquisaS3()
    {
        pesquisaS3.SetActive(true);
    } 

    //Pesquisas sobre Politica
    
    public void PesquisaP1()
    {
        pesquisaP1.SetActive(true);
    }
    public void PesquisaP2()
    {
        pesquisaP2 .SetActive(true);
    }
    public void PesquisaP3()
    {
        pesquisaP3 .SetActive(true);
    }

    //Pesquisas sobre Economia
    public void PesquisaE1()
    {
        pesquisaE1.SetActive(true);
    } 
    public void PesquisaE2()
    {
        pesquisaE2 .SetActive(true);
    }
    public void PesquisaE3()
    {
        pesquisaE3 .SetActive(true);
    }


}
