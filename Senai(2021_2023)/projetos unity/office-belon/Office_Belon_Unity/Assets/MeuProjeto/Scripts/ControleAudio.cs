using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Rendering;
using UnityEngine.UI;
public class ControleAudio : MonoBehaviour
{
    private static ControleAudio _instance;
    public static ControleAudio instance
    {
        get
        {
            return _instance;
        }
    }

    //[SerializeField] private AudioMixer aMixer;
    public AudioClip[] musicas;
    private AudioSource audioSource;
    //[SerializeField] private float volume;
    [SerializeField] private float volume;
    [SerializeField] private float trackTimer;
    [SerializeField] private float songsPlayed;
    [SerializeField] private bool[] beenPlayed;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        /*
        else
            Destroy(this.gameObject);
        */
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        beenPlayed = new bool[musicas.Length];

        if (!audioSource.isPlaying)
        {
            ChangeSong(Random.Range(0, musicas.Length));
        }

    }
    private void Update()
    {
        audioSource.volume = volume;

        if(audioSource.isPlaying)
        {
            trackTimer +=1*Time.deltaTime;
        }

        if (!audioSource.isPlaying|| trackTimer>= audioSource.clip.length || Input.GetKeyDown(KeyCode.Space))
        {
            ChangeSong(Random.Range(0, musicas.Length));
        }

        ResetShuffle();

        
    }

    public void ChangeSong(int songPicked)
    {
       if (!beenPlayed[songPicked])
        {
            trackTimer = 0;
            songsPlayed++;
            beenPlayed[songPicked] = true;
            audioSource.clip = musicas[songPicked];
            audioSource.Play();
        }
       else
        {
            audioSource.Stop(); 
        }

        
    }


     private void ResetShuffle()
    {
        if (songsPlayed == musicas.Length)
        {
            songsPlayed = 0;
            for (int i = 0; i < musicas.Length; i++)
            {
                if (i == musicas.Length)
                    break;
                else
                    beenPlayed[i] = false;
            }

        }
    }
    
 
    public void ChangeValue(Slider slider)
    {
        volume = slider.value;
        /*switch (volume)
        {
            case 0:
                aMixer.SetFloat("Musica", -88);
                break;

            case 1:
                aMixer.SetFloat("Musica", -40);
                break;
            case 2:
                aMixer.SetFloat("Musica", -20);
                break;
            case 3:
                aMixer.SetFloat("Musica", -10);
                break;
            case 4:
                aMixer.SetFloat("Musica", 0);
                break;
            case 5:
                aMixer.SetFloat("Musica", 10);
                break;

        }*/
    }
}
