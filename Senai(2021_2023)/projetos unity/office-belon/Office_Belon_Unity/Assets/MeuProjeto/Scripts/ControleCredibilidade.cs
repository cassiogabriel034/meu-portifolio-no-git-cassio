using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControleCredibilidade : MonoBehaviour
{
    [Header("Variáveis principais")]
    public GameObject telaFinalBoa;
    public GameObject telaFinalMediana;
    public GameObject telaFinalRuim;
    public int credibilidade;
    public Image barraCredibilidade;
    float atualBarraCredibilidade;
    private float maxBarraCredibilidade = 100;
    bool vitoria = false;
    bool derrota = false;
    bool fimDeJogo = false;

    void Start()
    {
        credibilidade = 60;
    }

    void Update()
    {  
        if (fimDeJogo == true)
        {
            if (credibilidade >= 90)
            {
                telaFinalBoa.SetActive(true);
                Debug.Log("tela final boa");
            }
            else if (credibilidade >30 && credibilidade < 90)
            {
                telaFinalMediana.SetActive(true);
                Debug.Log("Tela +ou-");
            }
            else if(credibilidade <=30)
            {
                telaFinalRuim.SetActive(true);
                Debug.Log("Tela ruim");
            }
        }  

        if(vitoria == true)
        {
            vitoria = false;
            credibilidade += 13;
        }

        if(derrota == true)
        {
            derrota = false;
            credibilidade -= 13;
        }

        atualBarraCredibilidade = credibilidade;
        barraCredibilidade.fillAmount = atualBarraCredibilidade/maxBarraCredibilidade;
    }

    public void Vitoria()
    {
        vitoria = true;
        Debug.Log("GANHOU CREDIBILIDADE");
        //vitoria = false;
    }

    public void Derrota()
    {
        derrota = true;
        Debug.Log("PERDEU CREDIBILIDADE");
        //derrota = false;
    }

    public void FinalDeJogo()
    {
        fimDeJogo = true;
        Debug.Log("true");
    }

    public void DiferentesFinais()
    {
        if (fimDeJogo == true)
        {
            if (credibilidade >= 90)
            {
                telaFinalBoa.SetActive(true);
                Debug.Log("tela final boa");
            }
            else if (credibilidade > 30 && credibilidade < 90)
            {
                telaFinalMediana.SetActive(true);
                Debug.Log("Tela +ou-");
            }
            else if (credibilidade <= 30)
            {
                telaFinalRuim.SetActive(true);
                Debug.Log("Tela ruim");
            }
        }
    }
}
