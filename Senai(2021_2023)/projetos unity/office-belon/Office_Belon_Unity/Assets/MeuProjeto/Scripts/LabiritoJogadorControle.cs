using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LabiritoJogadorControle : MonoBehaviour
{

    [Header("Movimento")]
    [SerializeField] Rigidbody2D rB;
    [SerializeField] float velocid;
    [SerializeField] Transform posicao;
    [SerializeField] Vector3 posicaoInicial;
    Vector3 posicaoMouseMundo;

    [Header("Entradas")]
    float entradaX;
    float entradaY;
    bool morte;
    Vector3 moveDirection;
    [SerializeField] float []tempoEspera;

    [Header("Objetos")]
    [SerializeField] GameObject blocoPreto;

    private void Start()
    {
        posicaoInicial = transform.position;
    }
    private void Update()
    {
        Entradas();
    }

    private void FixedUpdate()
    {
        Movimento();
    }

    void Entradas()
    {
        entradaX = Input.GetAxisRaw("Horizontal");
        entradaY = Input.GetAxisRaw("Vertical");
        
    }

    void Movimento()
    {
        rB.velocity = new Vector3(entradaX * velocid, entradaY * velocid, 0);
    }

    IEnumerator AtivaAposPeriodo(float tempo)
    {
        yield return new WaitForSeconds(tempo);
        blocoPreto.gameObject.SetActive(true);
    }

    IEnumerator VitoriaPuzzle(float tempo)
    {
        yield return new WaitForSeconds(tempo);
        ControlPuzzle.Instance.ControlAparênciaEmail();
    }

    void DesativacaoBlocoPreto()
    {
        blocoPreto.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("PontoDeChegada"))
        {
            ControlPuzzle.Instance.vitoriaPuzzleHac1 = true;
            StartCoroutine(VitoriaPuzzle(tempoEspera[1]));
        }

        if(collision.CompareTag("ObstaculosVermelho"))
        {
            Vector3 position = posicaoInicial;
            morte = true;
            if (morte == true)
            {
                blocoPreto.gameObject.SetActive(false);

                StartCoroutine(AtivaAposPeriodo(tempoEspera[0]));
            }
            transform.position = position;
            
        }
    }
}
