using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControleMenu : MonoBehaviour
{
    public static ControleMenu instance;

    public int telaFinal = 0;

    //[Header("Interfaces")]
    //[SerializeField] GameObject menuPause;
    //[SerializeField] GameObject menuVitoria;
    //[SerializeField] GameObject menuDerrota;
    //[SerializeField] GameObject menuConfiguracoes;
    //[SerializeField] GameObject menuAudio;
    //[SerializeField] GameObject menuLegenda;
    [SerializeField] GameObject menuPrimario;
    [SerializeField] GameObject menuPrincipal;
    //[SerializeField] GameObject menuEmail;

    //[Header("Noticias")]
    //[SerializeField] GameObject noticia1;
    //[SerializeField] GameObject noticia2;
    //[SerializeField] GameObject noticia3;
    //[SerializeField] GameObject noticia4;
    //[SerializeField] GameObject noticia5;

    //[Header("Sites")]
    //[SerializeField] GameObject site1;
    //[SerializeField] GameObject site2;

    //[Header("Entrevista")]
    //[SerializeField] GameObject imagemEntrevista;   
    //[SerializeField] GameObject imagemDaEntrevistaDoPrefeito;

    //[Header("Passar Noticia")]
    //private bool passarNoticia;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //menuAudio.SetActive(false);
        //menuConfiguracoes.SetActive(false);
        //menuDerrota.SetActive(false);
        //menuEmail.SetActive(false);
        //menuLegenda.SetActive(false);
        //menuPause.SetActive(false);
        menuPrimario.SetActive(true);
        menuPrincipal.SetActive(false);
        //menuVitoria.SetActive(false);   
    }

    public void Jogar()
    {
        SceneManager.LoadScene("CenaTutorial");
    }

    public void BtnVoltarMenuInicial()
    {
        SceneManager.LoadScene("CenaMenu"); 
    }

    public void BtnReiniciarCampanha()
    {
        SceneManager.LoadScene("CenaRodrigo");
    }

    public void BtnSair()
    {
        Application.Quit();
    }

    public void BtnIniciarJogo()
    {
        SceneManager.LoadScene("CenaRodrigo");
    }

    public void BtnPause()
    {
        //menuPause.SetActive(true);
    }

    public void BtnSairPause()
    {
        //menuPause.SetActive(false);
    }

    public void BtnConfiguracoes()
    {
        //menuConfiguracoes.SetActive(true);
        //menuPrincipal.SetActive(false);
        //menuPause.SetActive(false);
    }

    public void BtnSairConfiguracoes()
    {
        //menuConfiguracoes.SetActive(false);
        //menuPrincipal.SetActive(true );
        //menuPause.SetActive(true);

    }

    public void BtnAudio()
    {
        //menuAudio.SetActive(true);
        //menuConfiguracoes.SetActive(false);
    }

    public void BtnSairAudio()
    {
        //menuAudio.SetActive(false);
        //menuConfiguracoes.SetActive(true);
    }

    public void Legenda()
    {
        //menuLegenda.SetActive(true);
        //menuConfiguracoes.SetActive(false);
    }

    public void SairLegenda()
    {
        //menuLegenda.SetActive(false);
        //menuConfiguracoes.SetActive(true);
    }

    public void BtnJogar()
    {
        Debug.Log("Apareceu Menu");
        menuPrincipal.SetActive(true);
        menuPrimario.SetActive(false);
    }

    public void BtnEmail()
    {
        //menuEmail.SetActive(true);
    }

    public void BtnSairEmail()
    {
        //menuEmail.SetActive(false);
    }

    public void BtnSite1()
    {
        //site1.SetActive(true);
    }

    public void BtnSite2()
    {
        //site2.SetActive(true);
    }

    public void BtnNoticia1()
    {
        //noticia1.SetActive(true);   
    }

    public void BtnSairNoticia1()
    {
        //noticia1.SetActive(false);
    }

    public void BtnSairNoticia2()
    {
        //noticia2.SetActive(false);
    }

    public void BtnNoticia2()
    {
        //noticia3.SetActive(true);
    }

    public void BtnEntrevista()
    {
        //imagemEntrevista.SetActive(true);
    }

    public void BtnSolicitarEntrevista()
    {
        //imagemDaEntrevistaDoPrefeito.SetActive(true);
    }

    public void Item1()
    {
        telaFinal++;
    }

    public void TelaFinal()
    {
        if (telaFinal == 3)
        {
            //menuVitoria.SetActive(true);
        }
    }










}
